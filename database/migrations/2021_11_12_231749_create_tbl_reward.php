<?php

use App\Models\Rewards;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

class CreateTblReward extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_rewards')) {
            Schema::create('tbl_rewards', function (Blueprint $table) {
                $table->id('id_reward');
                $table->string('uuid_reward', 100);
                $table->string('reward', 200);
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            $this->insertData();
        }
    }

    private function insertData()
    {
        Rewards::insert(
            [
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Bantuan Video Call',
                ],
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Kesempatan main Jackpot',
                ],
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Bantuan tanya penonton',
                ],
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Bantuan buang 2 yang salah',
                ],
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Bonus Rp 1.000.000',
                ],
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Bonus Rp 2.000.000',
                ],
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Bonus Rp 3.000.000',
                ],
                [
                    'uuid_reward' => Uuid::uuid4(),
                    'reward' => 'Bonus Rp 5.000.000',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_rewards');
    }
}
