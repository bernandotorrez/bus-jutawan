<?php

use App\Models\Punishments;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

class CreateTblPunishments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_punishments')) {
            Schema::create('tbl_punishments', function (Blueprint $table) {
                $table->id('id_punishment');
                $table->string('uuid_punishment', 100);
                $table->string('punishment', 200);
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            $this->insertData();
        }

    }

    private function insertData()
    {
        Punishments::insert(
            [
                [
                    'uuid_punishment' => Uuid::uuid4(),
                    'punishment' => 'Kembali ke Pertanyaan Awal',
                ],
                [
                    'uuid_punishment' => Uuid::uuid4(),
                    'punishment' => 'Nominal Total di kurangi 50%',
                ],
                [
                    'uuid_punishment' => Uuid::uuid4(),
                    'punishment' => 'Dikurangi Rp 1.000.000',
                ],
                [
                    'uuid_punishment' => Uuid::uuid4(),
                    'punishment' => 'Dikurangi Rp 2.000.000',
                ],
                [
                    'uuid_punishment' => Uuid::uuid4(),
                    'punishment' => 'Dikurangi Rp 3.000.000',
                ],
                [
                    'uuid_punishment' => Uuid::uuid4(),
                    'punishment' => 'Dikurangi Rp 5.000.000',
                ],
                [
                    'uuid_punishment' => Uuid::uuid4(),
                    'punishment' => 'Lorem ipsum',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_punishments');
    }
}
