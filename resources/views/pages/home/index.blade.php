@extends('layouts.main')

@section('title', 'Home')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/dt-global_style.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery-ui.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/animate/animate.css')}}">
<style>
    .custom {
        width: 65px !important;
        height: 65px !important;
        border-color: red !important;
        font-size: 20px !important;
        box-shadow: 5px 5px 5px #607690;
    }

    .parallelogram {
        transform: skew(-15deg);
    }

    .btn-secondary:hover {
        background: gray;
    }

    .trapezoid {
        border-bottom: 50px !important;
        border-left: 25px !important;
        border-right: 25px !important;
    }

    .black-background {
        background-color: black !important;
    }

    .blockui-growl-message {
        display: none;
        text-align: left;
        padding: 15px;
        background-color: #455a64;
        color: #fff;
        border-radius: 3px;
    }
    .blockui-animation-container { display: none; }
    .multiMessageBlockUi {
        display: none;
        background-color: #455a64;
        color: #fff;
        border-radius: 3px;
        padding: 15px 15px 10px 15px;
    }
    .multiMessageBlockUi i { display: block }
</style>
@endpush

@section('content')
<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">

            <h6 class="text-center">Play</h6>
            <br>

            @php $no = 1; @endphp
            @foreach($rewardsAndPunishments as $chunk)
            <p>
                <div class="row text-center">
                    <div class="col-md-12">
                        <center>
                            @foreach($chunk as $data)
                                @if(array_key_exists('id_reward', $data))
                                    <button type="button" class="btn btn-secondary custom parallelogram mr-3 mb-3"
                                        onclick="showRewardOrPunishment(this)"
                                        data-id="{{ $data['uuid_reward'] }}"
                                        data-category='reward'
                                        data-clicked='0'
                                        >{{ $no++ }}</button>
                                @else
                                    <button type="button" class="btn btn-secondary custom parallelogram mr-3 mb-3"
                                        onclick="showRewardOrPunishment(this)"
                                        data-id="{{ $data['uuid_punishment'] }}"
                                        data-category="punishment"
                                        data-clicked='0'
                                        >{{ $no++ }}</button>
                                @endif

                            @endforeach
                        </center>
                    </div>

                </div>
            </p>
            @endforeach
        </div>

        {{-- Modal --}}
        <div class="modal fade black-background" id="rewardOrPunishmentModal" tabindex="-1" role="dialog"
            aria-labelledby="rewardOrPunishmentModalLabel" aria-modal="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <p>
                            <h1 class="title" id="message"></h1>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script src="{{ asset('assets/js/jquery-ui-1.12.1.js') }}"></script>
<script src="{{ asset('assets/js/shim.js')}}"></script>
<script src="{{ asset('plugins/blockui/jquery.blockUI.min.js') }}"></script>

<script>
    function showRewardOrPunishment(el) {
        // el.disabled = true
        var clicked = el.getAttribute('data-clicked');

        if(clicked == '0') {
            el.style.backgroundColor = "#786d83"
            el.setAttribute('data-clicked', '1');
        } else {
            getRewardOrPunishment(el);
        }

    }

    function getRewardOrPunishment(el) {
        var urlReward = "{{ route('home.reward') }}"
        var urlPunishment = "{{ route('home.punishment') }}"
        var id = el.getAttribute('data-id');
        var category = el.getAttribute('data-category');
        var url = (category == 'reward') ? urlReward : urlPunishment;
        url = url+'/'+id;
        console.log(category)
        if(category == 'reward') {
            $('#rewardOrPunishmentModal').addClass('purple-modal');
            $('#rewardOrPunishmentModal').removeClass('red-modal');
        } else {
            $('#rewardOrPunishmentModal').addClass('red-modal');
            $('#rewardOrPunishmentModal').removeClass('purple-modal');
        }

        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $.blockUI({
                    message: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-loader spin"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>',
                    fadeIn: 800,
                    overlayCSS: {
                        backgroundColor: '#191e3a',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#25d5e4',
                        zIndex: 1201,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $.unblockUI()
                $('#message').html(response.data)
                $('#rewardOrPunishmentModal').modal('show');
            },
            error: function(error) {
                $.unblockUI()
                alert('Something went wrong');
            },
        })
    }

    function openURL(el) {
        var urlReward = "{{ route('home.reward') }}"
        var urlPunishment = "{{ route('home.punishment') }}"
        var id = el.getAttribute('data-id');
        var category = el.getAttribute('data-category');
        var url = (category == 'reward') ? urlReward : urlPunishment;

        window.open(url+'/'+id, '_blank');
    }
</script>
@endpush
